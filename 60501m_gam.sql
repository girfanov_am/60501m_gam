-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Ноя 04 2020 г., 21:08
-- Версия сервера: 8.0.21-0ubuntu0.20.04.4
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60501m_gam`
--

-- --------------------------------------------------------

--
-- Структура таблицы `beauticians`
--

CREATE TABLE `beauticians` (
  `Beautician_ID` int UNSIGNED NOT NULL,
  `Beautician_name` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `beauticians`
--

INSERT INTO `beauticians` (`Beautician_ID`, `Beautician_name`) VALUES
(1, 'Иванова'),
(2, 'Петрова'),
(3, 'Сидорова'),
(4, 'Смирнова'),
(5, 'Кузнецова'),
(6, 'Васильева'),
(7, 'Федорова'),
(8, 'Михайлова'),
(9, 'Степанова'),
(10, 'Захарова');

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `Client_ID` int UNSIGNED NOT NULL,
  `Client_name` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`Client_ID`, `Client_name`) VALUES
(1, 'Ксения'),
(2, 'Анна'),
(3, 'Екатерина'),
(4, 'Дарья'),
(5, 'Александра'),
(6, 'Валентина'),
(7, 'Галина'),
(8, 'Любовь\r\n'),
(9, 'Наталья'),
(10, 'Елена');

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE `services` (
  `Service_ID` int UNSIGNED NOT NULL,
  `Service_name` varchar(20) DEFAULT NULL,
  `Service_price` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`Service_ID`, `Service_name`, `Service_price`) VALUES
(1, 'Чистка', 1000),
(2, 'Маска1', 3500),
(3, 'Маска2', 3200),
(4, 'Маска3', 3600),
(5, 'Пилинг', 7000),
(6, 'Пластика', 5500),
(7, 'Массаж', 4000),
(8, 'Маникюр', 3000),
(9, 'Маникюр1', 3000),
(10, 'Маникюр2', 3500);

-- --------------------------------------------------------

--
-- Структура таблицы `service_provided`
--

CREATE TABLE `service_provided` (
  `Session_ID` int UNSIGNED DEFAULT NULL,
  `Service_ID` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `service_provided`
--

INSERT INTO `service_provided` (`Session_ID`, `Service_ID`) VALUES
(5, 3),
(9, 7),
(4, 3),
(6, 7),
(7, 1),
(8, 1),
(6, 2),
(3, 4),
(1, 1),
(2, 3),
(5, 4),
(3, 3),
(5, 5),
(5, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `sessions`
--

CREATE TABLE `sessions` (
  `Session_ID` int UNSIGNED NOT NULL,
  `Client_ID` int UNSIGNED DEFAULT NULL,
  `Beautician_ID` int UNSIGNED DEFAULT NULL,
  `Begin_date` datetime DEFAULT NULL,
  `End_date` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1260 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `sessions`
--

INSERT INTO `sessions` (`Session_ID`, `Client_ID`, `Beautician_ID`, `Begin_date`, `End_date`) VALUES
(1, 1, 2, '2020-01-14 15:23:44', '2020-01-14 17:45:55'),
(2, 1, 3, '2020-01-13 15:22:48', '2020-01-13 17:45:39'),
(3, 1, 5, '2020-01-12 15:21:46', '2020-01-12 17:45:37'),
(4, 1, 4, '2020-01-11 15:20:55', '2020-01-11 17:45:35'),
(5, 1, 2, '2020-01-10 15:20:22', '2020-01-10 17:45:31'),
(6, 2, 1, '2020-02-06 17:13:25', '2020-02-06 19:16:26'),
(7, 3, 1, '2020-01-07 20:17:20', '2020-01-07 21:31:15'),
(8, 1, 2, '2020-01-09 15:20:00', '2020-01-09 17:45:31'),
(9, 1, 7, '2020-01-09 15:20:00', '2020-01-09 17:50:31'),
(10, 4, 7, '2020-01-09 18:00:32', '2020-01-09 18:50:31');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `beauticians`
--
ALTER TABLE `beauticians`
  ADD PRIMARY KEY (`Beautician_ID`);

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`Client_ID`);

--
-- Индексы таблицы `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`Service_ID`);

--
-- Индексы таблицы `service_provided`
--
ALTER TABLE `service_provided`
  ADD KEY `Service_ID` (`Service_ID`),
  ADD KEY `Session_ID` (`Session_ID`);

--
-- Индексы таблицы `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`Session_ID`),
  ADD KEY `Beautician_ID` (`Beautician_ID`),
  ADD KEY `Client_ID` (`Client_ID`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `beauticians`
--
ALTER TABLE `beauticians`
  MODIFY `Beautician_ID` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `clients`
--
ALTER TABLE `clients`
  MODIFY `Client_ID` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `services`
--
ALTER TABLE `services`
  MODIFY `Service_ID` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `sessions`
--
ALTER TABLE `sessions`
  MODIFY `Session_ID` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `service_provided`
--
ALTER TABLE `service_provided`
  ADD CONSTRAINT `service_provided_ibfk_1` FOREIGN KEY (`Session_ID`) REFERENCES `sessions` (`Session_ID`),
  ADD CONSTRAINT `service_provided_ibfk_2` FOREIGN KEY (`Service_ID`) REFERENCES `services` (`Service_ID`);

--
-- Ограничения внешнего ключа таблицы `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`Client_ID`) REFERENCES `clients` (`Client_ID`),
  ADD CONSTRAINT `sessions_ibfk_2` FOREIGN KEY (`Beautician_ID`) REFERENCES `beauticians` (`Beautician_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
